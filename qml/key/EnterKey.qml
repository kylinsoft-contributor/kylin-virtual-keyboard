import QtQuick 2.0

ActionKey {
    width: virtualKeyboard.keyWidth*2.5+virtualKeyboard.keySpacing*2
    alignment: Text.AlignRight
    label: "Enter"
    actionKeyImgPath: "qrc:/img/enter.svg"
}
