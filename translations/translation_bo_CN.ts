<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>VirtualKeyboardEntryManager</name>
    <message>
        <location filename="../src/virtualkeyboardentry/virtualkeyboardentrymanager.cpp" line="78"/>
        <source>Disable the float button</source>
        <translation>ཐིག་ཁྲམ་འཕུར་འཕུར་གྱི་རྐང་རྩེད་ཁ་དོག</translation>
    </message>
    <message>
        <location filename="../src/virtualkeyboardentry/virtualkeyboardentrymanager.cpp" line="85"/>
        <source>Enable the float button</source>
        <translation>ཐིག་ཁྲམ་སྟེང་གི་འཕུར་སྣང་ལ་འཇུག་པ།</translation>
    </message>
</context>
<context>
    <name>VirtualKeyboardTrayIcon</name>
    <message>
        <location filename="../src/virtualkeyboardentry/virtualkeyboardtrayicon.cpp" line="25"/>
        <source>kylin-virtual-keyboard</source>
        <translation>རྟོག་བཟོས་མཐེབ་གཞུང་།</translation>
    </message>
</context>
</TS>
